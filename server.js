/*
	This is the server for vidchatwith.me

	It should be hosted on a node.js host, with modules websocket and opentok installed.
	
	Ensure that port 3000 is open for traffic
	
	Claude Sutterlin
	5/1/2012
*/

"use strict";

// Optional. You will see this name in eg. 'ps' or 'top' command
process.title = 'vidChatWithme';

// Port where we'll run the websocket server
var webSocketsServerPort = process.env['app_port'] || 3000;

// websocket and http servers
var webSocketServer = require('websocket').server;
var http = require('http');
var opentok = require('opentok');
var ot =  new opentok.OpenTokSDK('14383161', 'bf9e2a721ac3137609c369a87749b3d2ada40874');

// list of currently connected clients (users)
var clients = [ ];

// list of keys (or meeting id's)
var keys = [];

/**
 * Helper function for escaping input strings
 */
function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;')
                      .replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

/**
 * HTTP server
 */
var server = http.createServer(function(request, response) {
    // Not important for us. We're writing WebSocket server, not HTTP server
});

server.listen(webSocketsServerPort, function() {
    console.log((new Date()) + " Server is listening on port " + webSocketsServerPort);
});

/**
 * WebSocket server
 */
var wsServer = new webSocketServer({
    // WebSocket server is tied to a HTTP server. To be honest I don't understand why.
    httpServer: server
});

// User object
function User(name, code){
	var self = this;
	
	this.initialize = function(connection){
		self.connection = connection;
		self.initialized = true;
	}
	
	this.sendChatSession = function(chatSession, role){
		self.chatSession = chatSession;
		self.role = role;
		
		self.pubtoken = self.chatSession.generatePubToken();
		self.token = self.chatSession.generateSubToken();
		self.chatSession.addUser(this);
		
		//create my message to return to the client
		var msg = {
			"type":"session_result",
			"key":self.chatSession.key,
			"sessionid":self.chatSession.session,
			"pubtoken":self.pubtoken,
			"subtoken":self.subtoken,
			"role":self.role,
			"members":self.chatSession.users.length
		}
		
		self.send(msg);					
	}
	
	this.notifyConnection = function(){
		//notify client that they were connected
		var result = {  type: 'connected' };							
		self.send(result);
	}
	
	this.send = function(obj){
		var json = JSON.stringify(obj);
		if (json){
			this.connection.sendUTF(json);	
		}
	}
		
	this.disconnect = function(){
	}		
	
	return this;
}

function ChatSession(){
	var self = {};
	self.key = '';
	self.users = [];
	self.session = {};
	
	self.init = function(){
		self.key = getKey();
	}
	
	self.createSession = function(callback){
		//set the session id
		ot.createSession('localhost', {}, function(session){
			self.session = session;	

			if (callback){
				callback();
			}
		});
	}
	
	//gets a publisher token from opentok api
	self.generatePubToken = function(){
		var pubtoken = ot.generateToken({sessionId: self.session,
										      role: opentok.RoleConstants.MODERATOR  });
		return pubtoken;	
	}
	
	//gets a subscriber token from opentok api
	self.generateSubToken = function(){
		var subtoken = ot.generateToken({sessionId: self.session,
											role: opentok.RoleConstants.SUBSCRIBER  });
		return subtoken;	
	}
	
	self.addUser = function(user){
		console.log("Added user" + user.connection);
		self.users.push(user);
		self.notifyUsers(user);
	}
	
	self.notifyUsers = function(user){
		var msg = {};
		console.log("Count: " + self.users.length);
		
		for (var i = 0; i<self.users.length; i++){
			var u = self.users[i];
			console.log("Looping user" + u.connection);
			
			if ((assigned(u)) && (u != user)){
				console.log("In loop");
				msg = {type: "new_user"};
				u.send(msg);
			}
		}
	}
	
	return self;
}

function assigned(obj){
	return (typeof obj !== 'undefined' && obj !== null);
}

//generates a public key
function getPubKey(subkey){
	//pub key is the sub key with two more digits appended with a -
	var s = [];
	
	console.log("subkey:" + s);

	var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 9; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
	
	s[6] = "-";
	
	var uuid=s.join("");
	
	console.log("pubkey:" + uuid);
	
	//before we return it we'll see if this key already exists
	if (assigned(keys[uuid])){
		uuid==getPubKey(subkey); //this has the potential to endlessly recurse, but only when we max out the connections
	}
	
	return uuid;
}

//generates a private key
function getKey(){
	// http://www.ietf.org/rfc/rfc4122.txt
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 6; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }

    var uuid = s.join("");
	
	//before we return it we'll see if this key already exists
	if (assigned(keys[uuid])){
		uuid==getKey(); //this has the potential to endlessly recurse, but only when we max out the connections
	}
	
    return uuid;
}

// This callback function is called every time someone
// tries to connect to the WebSocket server
wsServer.on('request', function(request) {
    console.log((new Date()) + ' Connection from origin ' + request.origin + '.');

	// accept connection - you should check 'request.origin' to make sure that
    // client is connecting from your website
    // (http://en.wikipedia.org/wiki/Same_origin_policy)
    var connection = request.accept(null, request.origin); 
	
	var user = new User();
	user.initialize(connection);
	user.notifyConnection();	

    // user sent some message
    connection.on('message', function(message) {
		// accept only text
        if (message.type != 'utf8') { 
			return false;
		}
		
		var msg = JSON.parse(message.utf8Data);
		
		if (msg.type == 'new_session'){
			//generate a new chat session
			var chat = new ChatSession();
			chat.init();
			
			chat.createSession(function(){
				user.sendChatSession(chat, "Publisher");
			});
			
			keys[chat.key] = chat;
		}
		else if (msg.type == 'join_session'){
			var key = msg.key;
			
			// see if the key is valid
			if (assigned(keys[key])){
				var chat = keys[key];
				
				var msg = {type: "join_result",
						   result: "true",
						   message: "Joining the meeting..."
						  }
				user.send(msg);
				
				// sends the session info
				user.sendChatSession(chat, "subscriber");
			}
			else{
				var msg = {type: "join_result",
						   result: "false",
						   message: "That is not a valid meeting code"
						  }
				user.send(msg);
				console.log("not found");
			}		
		}
			
    });

    // user disconnected
    connection.on('close', function(connection) {
        if ((assigned(user)) && (user.initialized)) {
            //console.log((new Date()) + " Peer "
             //   + user.connection.remoteAddress + " disconnected.");
				
			user.disconnect();
			clients[user.userName] == false;
        }
    });
});